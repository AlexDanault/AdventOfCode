//Libs core
const util = require('util');
const fs = require('fs');

//Promisify
const promisify = util.promisify;

//Rendre async les foncitons callback du core
const readFileAsync = promisify(fs.readFile);

async function main() {
  const input = '' + await readFileAsync('input.txt'); // Le " '' + " c'est pour convertir le buffer en string

  //Casser en lignes
  const chars = input.split('');

  await partOne(chars);
  await partTwo(chars);
};

async function partOne(chars) {
  console.info('Partie 1...');

  //copy first item last
  const localChars = chars.slice(); //safe copy
  localChars.push(localChars[0]);

  let sum = 0;
  localChars.forEach((char, index) => {
    if (char == localChars[index+1]) {
      sum += parseInt(char);
    }
  });

  console.log('Sum: ' + sum.toString());
};

async function partTwo(chars) {
  console.info('Partie 2...');

  let sum = 0, mid = chars.length / 2;

  chars.forEach((char, index) => {
    if (((index < mid) && (char == chars[index + mid])) || ((index >= mid) && (char == chars[index - mid]))) {
      sum += parseInt(char);
    }
  });

  console.log('Sum: ' + sum.toString());
};

//Exécuter les parties
main();