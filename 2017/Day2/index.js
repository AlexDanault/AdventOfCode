//Libs core
const util = require('util');
const fs = require('fs');

//Promisify
const promisify = util.promisify;

//Rendre async les foncitons callback du core
const readFileAsync = promisify(fs.readFile);

async function main() {
  const input = '' + await readFileAsync('input.txt'); // Le " '' + " c'est pour convertir le buffer en string

  //Casser en lignes
  const lines = input.split('\n');

  await partOne(lines);
  await partTwo(lines);
};

async function partOne(lines) {
  console.info('Partie 1...');

  let sum = 0;

  lines.forEach(line => {
    let min = 999999, max = 0, diff = 0;

    let cols = line.split('\t');

    cols.forEach(cell => {
      let iCell = parseInt(cell);

      if (iCell < min) {
        min = iCell;
      };

      if (iCell > max) {
        max = iCell;
      };
    });

    diff = max - min;
    sum += diff;

    console.log('min', min, 'max', max, 'diff', diff, 'sum', sum);
  });

  console.log('final sum', sum);
};

async function partTwo(lines) {
  console.info('Partie 2...');

  let sum = 0;

  lines.forEach(line => {
    let div = 0;

    let cols = line.split('\t');

    cols.forEach(cell1 => {
      let iCell1 = parseInt(cell1);

      cols.forEach(cell2 => {
        let iCell2 = parseInt(cell2);

        if ((iCell1 != iCell2) && ((iCell1 / iCell2) == Math.floor(iCell1 / iCell2))) {
          if (iCell1 < iCell2) {
            div = (iCell2 / iCell1);
          } else {
            div = (iCell1 / iCell2);
          }

          console.log('one', iCell1, 'two', iCell2, 'div', div);
        };
      });
    });

    sum += div;
  });

  console.log('final sum', sum);
};

//Exécuter les parties
main();