//Libs core
const util = require('util');
const fs = require('fs');

//Promisify
const promisify = util.promisify;

//Rendre async les foncitons callback du core
const readFileAsync = promisify(fs.readFile);

async function main() {
  const input = '' + await readFileAsync('input.txt'); // Le " '' + " c'est pour convertir le buffer en string

  //Casser en lignes
  const lines = input.split('\n');

  await partOne(lines);
  await partTwo(lines);
};

async function partOne(lines) {
  console.info('Partie 1...');

  let pos = 5;
  let res = '';

  lines.forEach(line => {
    const chars = line.split('');

        chars.forEach(char => {
          if (char == 'U' && pos > 3) {
            pos -= 3;
          } else if (char == 'D' && pos < 7) {
            pos += 3;
          } else if (char == 'L' && pos != 1 && pos != 4 && pos != 7) {
            pos -= 1;
          } else if (char == 'R' && pos != 3 && pos != 6 && pos != 9) {
            pos += 1;
          };
        });

        res += pos.toString();
  });

  console.log(res);
  console.info('');
};

async function partTwo(lines) {
  console.info('Partie 2...');

  const pad = [
    [' ', ' ', ' ', ' ', ' '],
    [' ', ' ', '1', ' ', ' '],
    [' ', '2', '3', '4', ' '],
    ['5', '6', '7', '8', '9'],
    [' ', 'A', 'B', 'C', ' '],
    [' ', ' ', 'D', ' ', ' '],
    [' ', ' ', ' ', ' ', ' '],
  ];

  console.log(pad);

  //Trouver la position du 5 pour le départ
  let x, y, i;

  pad.forEach((padLine, padLineIndex) => {
    i = padLine.indexOf('5');

    if (i > -1) {
      x = padLineIndex;
      y = i;
    }
  });

  console.log('Starting at X: ' + x + ' Y: ' + y + ' on: "' + pad[x][y] + '"');

  console.info('');
};

//Exécuter les parties
main();